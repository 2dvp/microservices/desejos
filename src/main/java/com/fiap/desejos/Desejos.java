package com.fiap.desejos;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "DESEJOS")
public class Desejos {
    @Id
    @GeneratedValue
    public int id;
    public int product_id; 
    public int type_id;
    public Integer getId() {
       return id;
   } 
}