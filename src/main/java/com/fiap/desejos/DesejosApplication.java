package com.fiap.desejos;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DesejosApplication {

	public static void main(String[] args) {
		SpringApplication.run(DesejosApplication.class, args);
	}

}
