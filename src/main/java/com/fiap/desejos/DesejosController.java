package com.fiap.desejos;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DesejosController {
    @Autowired
    DesejosService desejosService;

    @GetMapping("/desejos")
    private List<Desejos> getAllViews() {
        return desejosService.getAllViews();
    }

    @GetMapping("/desejos/{id}")
    private Desejos getView(@PathVariable("id") int id) {
        return desejosService.getViewById(id);
    }

    @DeleteMapping("/desejos/{id}")
    private void deleteView(@PathVariable("id") int id) {
      desejosService.delete(id);
    }

    @PostMapping("/desejos")
    private int saveView(@RequestBody Desejos desejos) {
      desejosService.saveOrUpdate(desejos);
        return desejos.getId();
    }
}