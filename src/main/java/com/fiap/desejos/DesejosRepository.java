package com.fiap.desejos;

import org.springframework.data.repository.CrudRepository;

public interface DesejosRepository extends CrudRepository<Desejos, Integer> {
    
}