package com.fiap.desejos;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DesejosService {
    @Autowired
    DesejosRepository desejosRepository;

    public List<Desejos> getAllViews() {
        List<Desejos> desejo = new ArrayList<Desejos>();
        desejosRepository.findAll().forEach(desejos -> desejo.add(desejos));
        return desejo;
    }

    public Desejos getViewById(int id) {
        return desejosRepository.findById(id).get();
    }

    public void saveOrUpdate(Desejos view) {
        desejosRepository.save(view);
    }


    public void delete(int id) {
        desejosRepository.deleteById(id);
    }    

}